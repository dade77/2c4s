package com.company;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        int i = 1;
        String line = null;
        //Scanner in = new Scanner(System.in);

        File file1 =new File("in.txt");
        try
        {
            boolean created = file1.createNewFile();
            if(created)
                System.out.println("File has been created");
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        try(FileWriter writer = new FileWriter("in.txt", false))
        {
            // запись всей строки
            String text = "32 + 11";
            writer.write(text);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        File file2 = new File("in.txt");
        try {
            Scanner input = new Scanner(file2);
            while (input.hasNextLine()){
                line=input.nextLine();
            }
            System.out.println(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String[] lineParts = line.split(" ");
        int a=Integer.parseInt(lineParts[0]);
        int b=Integer.parseInt(lineParts[2]);
        String x=lineParts[1];


        while (i == 1) {
            float out = 0;

            /*System.out.print("Введите a=");
            float a = in.nextFloat();
            System.out.print("Введите b=");
            float b = in.nextFloat();
            System.out.print("Введите x=");
            String x = in.next();*/

            if ("+".equals(x)) {
                out = a + b;
            } else if ("-".equals(x)) {
                out = a - b;
            } else if ("*".equals(x)) {
                out = a * b;
            } else if ("/".equals(x)) {
                out = a / b;
            } else {
                System.out.println("Нет такого действия");
            }

            System.out.println("Получаем=" + out);

            //System.out.print("Введите 1 для продолжения или 0 для выхода");
            i = 0; //in.nextInt();
            a=0;
            b=0;
            x="";


        }
        //in.close();
    }

}


package ru.penzgtu1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.util.Arrays;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);

    }

    @Override public void start(Stage stage) {


        int n=25;
        int x=0;
        int i=0;

        int mas[]=new int[25];
        int time[]=new int[25];

        for (i=0; i<n; i++)
        {
            mas[i]=i+10;
        }

        for (i=0; i<n; i++)
        {
            time[i]=i+1;
        }
        int j=0;
        int w=3;//Размер окна
        int v=0;
        int h=0;
        i=-n^2;
        int window[]=new int[w];
        int suma=0;
        int summ[]=new int[n];
        int calc=0;
        int schetchickf=0;

        while (i<=n)
        {
            if (j<=w-1 && v<=n-1)
            {
                window[j]=mas[v];
                suma+=window[j];
                calc=suma/w;
                j+=1;
                v+=1;
                summ[schetchickf]=calc;
                schetchickf++;
            }
            else j=0;
            i++;

        }
        System.out.println(Arrays.toString(summ));

        stage.setTitle("SMA");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Time");
        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);

        lineChart.setTitle("SMA");
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName("SMA");
        //populating the series with data
        for (int ij = 0; ij < 25; ij++) {
            series.getData().add(new XYChart.Data(time[ij], summ[ij]));
        }

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("BASE VAL");
        for (int ji = 0; ji < 25; ji++) {
            series1.getData().add(new XYChart.Data(time[ji], mas[ji]));
        }

        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series, series1);

        stage.setScene(scene);
        stage.show();

    }
}

package ru.penzgtu1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main extends Application {
    final static String austria = "Austria";
    final static String brazil = "Brazil";
    final static String france = "France";
    final static String italy = "Italy";
    final static String usa = "USA";

    final String path = "C:\\projects\\yap\\2c4s\\Lab5_Spam\\mbox.txt";

    public static void main(String[] args) {
        launch(args);

    }
    @Override public void start(Stage stage) {

        try(BufferedReader br = new BufferedReader(new FileReader(path)))
        {
            //чтение построчно
            String s;
            Pattern pattern = Pattern.compile("\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*\\.\\w{2,4}");
            Matcher matcher;
            ArrayList<String> author = new ArrayList<String>();
            ArrayList <String> author_clear = new ArrayList<String>();

            while((s=br.readLine())!=null){
                if (s.contains("From:"))
                {
                    matcher = pattern.matcher(s);
                    if (matcher.find()) {
                        author.add(matcher.group());
                        //System.out.println(matcher.group());
                    }


                }

            }
            br.close();
            String[] author_mas=author.toArray(new String[author.size()]);
            Arrays.sort(author_mas);

            for (int i=0; i<author.size(); i++)
            {
                int g=i+1;
                while (g<author.size())
                {
                    if (author.get(i).equals(author.get(g)))
                    {
                        author.remove(g);
                        g-=1;
                    }
                    g++;
                }
            }

            int i=0;
            int j=0;
            ArrayList <Float> coefficient = new ArrayList<Float>();
            HashMap<String, Float> caef_for_user = new HashMap<>();
            HashMap<String, Integer> mail_for_user = new HashMap<>();
            ArrayList <Float> esche_coef = new ArrayList<Float>();

            s=null;
            String s1;
            String[] coe;
            int mail_c=0;


            while (j<author.size())
            {
                BufferedReader br1 = new BufferedReader(new FileReader(path));
                int calc=0;
                while((s=br1.readLine())!=null)
                {
                    if(s.equals("From: "+author.get(j))) {
                        while ((s1 = br1.readLine())!=null) {
                            if (s1.contains("X-DSPAM-Confidence:")) {
                                coe = s1.split(" ");
                                coefficient.add(Float.parseFloat(coe[1]));
                                mail_c++;
                                break;
                            }

                        }
                    }
                }
                if(mail_c!=0)
                {
                    float yahz = 0;
                    for (int ji = 0; ji < coefficient.size(); ji++) {
                        yahz += coefficient.get(ji);
                    }
                    float coef_hz = yahz / coefficient.size();
                    caef_for_user.put(author.get(j), coef_hz);
                    mail_for_user.put(author.get(j), mail_c);
                    mail_c = 0;
                    coef_hz=0;
                }


                j+=1;
                br1.close();
                coefficient.clear();
            }
            System.out.println(caef_for_user);
            System.out.println(mail_for_user);




            stage.setTitle("Bar Chart Sample");
            final CategoryAxis xAxis = new CategoryAxis();
            final NumberAxis yAxis = new NumberAxis();
            final BarChart<String,Number> bc =
                    new BarChart<>(xAxis,yAxis);
            bc.setTitle("Спам");
            xAxis.setLabel("Адреса");
            yAxis.setLabel("Коэффициент");

            XYChart.Series series1 = new XYChart.Series();
            //series1.setName("2003");

            ArrayList<String> keys = new ArrayList<>(caef_for_user.keySet());
            for (int q=0; q<caef_for_user.size(); q++) {
                series1.getData().add(new XYChart.Data(keys.get(q), caef_for_user.get(keys.get(q))));
            }


            Scene scene  = new Scene(bc,800,600);
            bc.getData().addAll(series1);
            stage.setScene(scene);
            stage.show();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());

        }
    }
}
